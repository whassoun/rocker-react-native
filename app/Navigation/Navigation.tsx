import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { screenOptions } from './NavigationStyles';
import { StackParamList } from './NavigationStackRouteProps';

import HomeScreen from '../Components/HomeScreen/HomeScreen';
import DetailScreen from '../Components/DetailScreen/DetailScreen';

const Stack = createStackNavigator<StackParamList>();

const AppNavigationContainer = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="HomeScreen" screenOptions={screenOptions}>
                <Stack.Screen name="HomeScreen" options={{ title: 'CoinData' }} component={HomeScreen} />
                <Stack.Screen name="DetailScreen"  options={({ route }) => ({ title: route.params.item.name })} component={DetailScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigationContainer;