export type StackParamList = {
    HomeScreen: undefined;
    DetailScreen: {
        item: {
            name: string,
            asset_id: string
        }
    };
};