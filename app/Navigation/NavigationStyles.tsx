import { StackNavigationOptions } from '@react-navigation/stack';

export const screenOptions: StackNavigationOptions = {
    headerShown: true,
    headerTitleAlign: 'center',
}