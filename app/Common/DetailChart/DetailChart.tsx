import React from 'react';
import { View } from 'react-native';
import styles from './DetailChartStyles';
import LoadingScreen from '../../Common/LoadingScreen/LoadingScreen';
import ErrorScreen from '../../Common/ErrorScreen/ErrorScreen';
import { LineChart, Grid, YAxis } from 'react-native-svg-charts'

const DetailChart = ({ data }) => {
    if (!data) return (<LoadingScreen />);
    if (!data.length) return (<ErrorScreen text="No values for this asset." />);
    return (
        <View style={styles.container}>
            <YAxis
                style={styles.yAxis}
                data={data}
                contentInset={styles.contentInset}
                svg={{
                    fill: 'grey',
                    fontSize: 10,
                }}
                numberOfTicks={10}
                formatLabel={(value) => `${value}$`}
            />
            <LineChart
                style={styles.lineChart}
                data={data}
                svg={{ stroke: 'rgb(134, 65, 244)' }}
                contentInset={styles.contentInset}
            >
                <Grid />
            </LineChart>
        </View>
    );
}

export default DetailChart;