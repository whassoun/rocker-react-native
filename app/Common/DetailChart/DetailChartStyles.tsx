import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        height: 400, 
        flexDirection: 'row' 
    },
    yAxis: {
        backgroundColor: '#eeeeee', 
        marginRight: 1
    },
    lineChart: {
        height: 400, 
        width: '100%', 
        backgroundColor: '#eeeeee'
    },
    contentInset: {
        top: 20,
        bottom: 20
    }
});
