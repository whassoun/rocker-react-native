import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native'; 

import styles from './LoadingScreenStyles';

const LoadingScreen = () => {
    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="#0000ff" />
        </View>
    );
}

export default LoadingScreen;