import React from 'react';
import { Text, View} from 'react-native'; 

import styles from './ErrorScreenStyles';

const ErrorScreen = ({ text }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{text}</Text>
        </View>
    );
}

export default ErrorScreen;