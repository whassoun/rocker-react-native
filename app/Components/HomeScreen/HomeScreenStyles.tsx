import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		backgroundColor: '#eee',
		flex: 1,
		padding: 10
	},
	textInput: { 
		backgroundColor: '#fff', 
		padding: 10,
		borderRadius: 10,
		borderWidth: 1,
		borderColor: '#eee'
	},
	item: {
		borderBottomWidth: 2,
		borderColor: '#eee'
	},
	touchable: {
		padding: 20
	},
	title: {
		fontSize: 18
	},
});
