import React, { useState, useCallback, useEffect } from 'react';
import { SafeAreaView, FlatList, Text, View, TouchableOpacity, TextInput } from 'react-native';
import { useQuery } from '@apollo/client';
import LoadingScreen from '../../Common/LoadingScreen/LoadingScreen';
import ErrorScreen from '../../Common/ErrorScreen/ErrorScreen';
import { ASSETS_DATA } from '../../Graphql/Queries';
import styles from './HomeScreenStyles';

const filterData = (searchValue, data) => {
    return data.filter(item => {
        let { name } = item;
        return name ? name.toLowerCase().includes(searchValue.toLowerCase()) : false;
    });
}

const LineItem = ({ item, navigation }) => {
    const { name } = item;
    return (
        <View style={styles.item}>
            <TouchableOpacity style={styles.touchable} onPress={() => navigation.navigate('DetailScreen', { item: item })}>
                <Text style={styles.title}>{name}</Text>
            </TouchableOpacity>
        </View>
    );
}

const HomeScreen = ({ navigation }) => {
    const { data, error, loading } = useQuery(ASSETS_DATA);
    const [searchValue, setSearchValue] = useState('');
    const [graphData, setGraphData] = useState([]);

    useEffect(() => {
        if (!data) return;
        let filteredGraphData = filterData(searchValue, data.getAssets);
        setGraphData(filteredGraphData);
    }, [data, searchValue])

    const renderItem = ({ item }) => (
        <LineItem item={item} navigation={navigation} />
    );

    if (loading) return (<LoadingScreen />);
    if (error) return (<ErrorScreen text={error.message} />);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={graphData}
                ListHeaderComponent={
                    <View style={styles.header}>
                        <TextInput
                            onChangeText={setSearchValue}
                            value={searchValue}
                            placeholder="Search coins"
                            clearButtonMode="always"
                            numberOfLines={1}
                            autoCapitalize="none"
                            autoCorrect={false}
                            style={styles.textInput}
                        />
                    </View>}
                renderItem={renderItem}
                keyExtractor={(item) => item.asset_id}
            />
        </SafeAreaView>
    );
}

export default HomeScreen;