import React, { useState, useEffect } from 'react';
import { View } from 'react-native';
import styles from './DetailScreenStyles';
import { useQuery } from '@apollo/client';
import LoadingScreen from '../../Common/LoadingScreen/LoadingScreen';
import ErrorScreen from '../../Common/ErrorScreen/ErrorScreen';
import { CHART_DATA } from '../../Graphql/Queries';
import DetailChart from '../../Common/DetailChart/DetailChart';

const DetailScreen = ({route}) => {
    const [chartData, setChartData] = useState([]);
    const { name, asset_id } = route.params.item;
    const { data, error, loading } = useQuery(CHART_DATA, {
        variables: {
            assetId: asset_id
        }
    });

    useEffect(() => {
        if (!data) return;
        let chartData = getChartData(data.getHistoricalCoinData);
        setChartData(chartData);
    }, [data]);

    const getChartData = (data) => {
        return data.map(({ price_close }) => price_close);
    }

    if (loading) return (<LoadingScreen />);
    if (error) return (<ErrorScreen text={error.message} />);

    return (
        <View style={styles.container}>
            <DetailChart data={chartData} />
        </View>
    );
}

export default DetailScreen;