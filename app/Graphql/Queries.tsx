import { gql } from '@apollo/client';

export const COIN_DATA = gql`
    query ($assetId: String!) {
        getHistoricalCoinData(assetId: $assetId)
        {
            time_period_start
            time_period_end,
            time_open,
            time_close,
            price_open,
            price_high,
            price_low,
            price_close,
            volume_traded,
            trades_count
        }
    }
`;

export const CHART_DATA = gql`
    query ($assetId: String!) {
        getHistoricalCoinData(assetId: $assetId)
        {
            time_period_start
            price_close,
        }
    }
`;

export const ASSETS_DATA = gql`
    query {
        getAssets {
            asset_id,
            name
        }
    }
`;