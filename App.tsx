import React from 'react';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import AppNavigationContainer from './app/Navigation/Navigation';

const client = new ApolloClient({
	uri: 'http://10.0.2.2:4000/',
	cache: new InMemoryCache()
});

export default function App() {
	return (
		<ApolloProvider client={client}>
			<AppNavigationContainer />
		</ApolloProvider>
	);
}